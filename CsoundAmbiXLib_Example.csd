<CsoundSynthesizer>


<CsOptions>
-o "test.wav"
</CsOptions>
<CsInstruments>
sr=96000
ksmps=32
0dbfs=1
nchnls=9
#include "CsoundAmbiXLib.txt"
;remember to set nchnls


;example using ENCODING1 UDO for 1st ambisonic order 
	instr 1
idur=p3
;AZYMUTH
ia = p4
;ELEVATION
ie = p5
ain oscil 1, 1
ka line ia, idur, ia
ke line ie, idur, ie
a0,a1,a2,a3 ENCODING1 ain,ka,ke
outs a0,a1,a2,a3
	endin

;example using ENCODE2 MACRO to implicitly invoke ENCODING2 UDO for 2nd ambisonic order
	instr 2
idur=p3
;AZYMUTH
ia = p4
;ELEVATION
ie = p5
ain oscil 1, 1
ka line ia, idur, ia
ke line ie, idur, ie
$ENCODE2(ain'ka'ke)
outs a0,a1,a2,a3,a4,a5,a6,a7,a8
	endin

</CsInstruments>
<CsScore>

i1	0 2	0		0

i1 + . 	90 0

i1 + . 	180 0

i1 + . 	270 0

i1 + . 	0 90

i1 + . 	0 180

i1 + . 	0 270

e
i2	0 2	0		0

i2 + . 	90 0

i2 + . 	180 0

i2 + . 	270 0

i2 + . 	0 90

i2 + . 	0 180

i2 + . 	0 270

</CsScore>


</CsoundSynthesizer>