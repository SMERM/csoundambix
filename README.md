# CsoundAmbiXLib

CsoundAmbiXLib is an UDO and MACRO library for ambisonics compositing on Csound.

## Installation

put the file in the INCDIR (Include Files Directory) folder chosen in the csound IDE settings.

## Usage

```csound
#include "CsoundAmbiXlib.txt"

;example of second order ambisonic encoder
	instr 1
idur=p3
;AZYMUTH
ia = p4
ifa = p5
;ELEVATION
ie = p6
ife = p7
ain oscil 1, 1
ka line ia, idur, fa
ke line ie, idur, fe
a0,a1,a2,a3,a4,a5,a6,a7,a8 ENCODING2 ain,ka,ke
outs a0,a1,a2,a3,a4,a5,a6,a7,a8
	endin
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.
